package com.yunqi.starter.redis.configuration;

import com.yunqi.starter.core.constant.GlobalConstant;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 配置类 Model
 * <p>
 * 你可以通过yml、properties、java代码等形式配置本类参数
 * Created by @author CHQ on 2023/9/4
 */
@Data
@ConfigurationProperties(prefix = RedisProperties.PREFIX)
public class RedisProperties {

    public static final String PREFIX = GlobalConstant.PREFIX + "redis";

    /** 是否开启 */
    boolean enabled = true;

    /** 订单号前缀 */
    private String orderNoPrefix;

    /** 是否打印操作日志 */
    private Boolean log = false;

}
