package com.yunqi.starter.redis.configuration;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 配置类 Model
 * <p>
 * 你可以通过yml、properties、java代码等形式配置本类参数
 * Created by @author CHQ on 2023/9/4
 */
@Data
@Accessors(chain = true)
public class RedisConfig implements Serializable {

    private static final long serialVersionUID = 1L;

    /** 订单号前缀 */
    private String orderNoPrefix;

    /** 是否打印操作日志 */
    private Boolean log = true;

}
