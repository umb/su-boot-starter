package com.yunqi.starter.dingtalk.configuration;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 配置类 Model
 * <p>
 * 你可以通过yml、properties、java代码等形式配置本类参数
 * Created by @author CHQ on 2022/4/1
 */
@Data
@Accessors(chain = true)
public class DingtalkConfig {

    private static final long serialVersionUID = 1L;

    /** API接口地址 */
    private String domain = "https://oapi.dingtalk.com";

    /** API接口地址 */
    private String domain2 = "https://api.dingtalk.com";

    /** 应用KEY */
    private String appKey = "";

    /** 应用密钥 */
    private String appSecret = "";

    /** AgentID */
    private String agentId = "";

    /** 是否打印操作日志 */
    private Boolean isLog = true;

}
