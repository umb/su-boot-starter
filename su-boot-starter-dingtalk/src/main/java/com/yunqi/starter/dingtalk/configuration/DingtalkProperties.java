package com.yunqi.starter.dingtalk.configuration;

import com.yunqi.starter.core.constant.GlobalConstant;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Created by @author CHQ on 2022/4/1
 */
@Data
@ConfigurationProperties(prefix = DingtalkProperties.PREFIX)
public class DingtalkProperties {

    public static final String PREFIX = GlobalConstant.PREFIX + "dingtalk";

    /** 是否开启 */
    boolean enabled = true;

    /** 应用KEY */
    private String appKey = "";

    /** 应用密钥 */
    private String appSecret = "";

    /** AgentID */
    private String agentId = "";

    /** 是否打印操作日志 */
    private Boolean log = false;
}
