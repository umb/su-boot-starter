package com.yunqi.starter.dingtalk.provider;

import com.yunqi.starter.core.lang.util.NutMap;

/**
 * 宜搭表单接口层
 * create by @author XZH on 2024/7/2
 */

public interface IDingtalkYiDaFormApi {

    /**
     * 查询表单实例数据
     * @param data 请求参数
     * @return 返回结果
     * @see <a href="https://open.dingtalk.com/document/orgapp/querying-form-instance-data">文档网址</a>
     */
    NutMap queryAllFormData(NutMap data);

    /**
     * 根据表单实例ID查询表单数据
     * @param requestParam  请求地址携带参数
     * @return              返回结果
     * @see <a href="https://open.dingtalk.com/document/orgapp/query-form-data">文档网址</a>
     */
    String queryFormData(String requestParam);

    /**
     * 更新表单数据
     * @param data  请求参数
     *              {
     *                  "appType" : "String",
     *                  "systemToken" : "String",
     *                  "userId" : "String",
     *                  "language" : "String",
     *                  "formInstanceId" : "String",
     *                  "useLatestVersion" : Boolean,
     *                  "updateFormDataJson" : "String"
     *              }
     * @return      返回结果
     * @see <a href="https://open.dingtalk.com/document/orgapp/update-form-data">文档网址</a>
     */
    NutMap updateFormsData(NutMap data);
}
