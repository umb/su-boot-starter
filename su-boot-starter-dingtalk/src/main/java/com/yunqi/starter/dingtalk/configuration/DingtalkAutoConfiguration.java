package com.yunqi.starter.dingtalk.configuration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

/**
 * 自动装配
 * Created by @author CHQ on 2022/4/1
 */
@Slf4j
@Configuration
@ConditionalOnExpression("${" + DingtalkProperties.PREFIX + ".enabled:true}")
@EnableConfigurationProperties(DingtalkProperties.class)
public class DingtalkAutoConfiguration {


    @Bean
    @Primary
    public DingtalkConfig getDingtalkConfig(DingtalkProperties properties) {
        if(properties.getLog()){
            log.info("钉钉组件 -> 开启组件");
        }
        return  new DingtalkConfig()
                .setAppKey(properties.getAppKey())
                .setAppSecret(properties.getAppSecret())
                .setAgentId(properties.getAgentId())
                .setIsLog(properties.getLog());
    }
}
