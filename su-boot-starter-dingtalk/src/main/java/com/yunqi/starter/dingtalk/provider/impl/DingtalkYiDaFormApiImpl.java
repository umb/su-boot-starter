package com.yunqi.starter.dingtalk.provider.impl;

import com.yunqi.starter.core.json.Json;
import com.yunqi.starter.core.lang.util.NutMap;
import com.yunqi.starter.dingtalk.provider.IDingtalkYiDaFormApi;
import com.yunqi.starter.dingtalk.util.DingtalkUtil;

/**
 * 宜搭表单接口实现层
 * create by @author XZH on 2024/7/2
 */
public class DingtalkYiDaFormApiImpl implements IDingtalkYiDaFormApi {
    @Override
    public NutMap queryAllFormData(NutMap data) {
        return DingtalkUtil.post2("/v1.0/yida/forms/instances/search", data);
    }

    @Override
    public String queryFormData(String requestParam) {
        NutMap res = DingtalkUtil.get3("/v1.0/yida/forms/instances/"+requestParam);
        return Json.toJson(res);
    }

    @Override
    public NutMap updateFormsData(NutMap data) {
        return DingtalkUtil.put("/v1.0/yida/forms/instances", data);
    }

}
