package com.yunqi.starter.weixin.configuration;

import com.yunqi.starter.core.constant.GlobalConstant;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Created by @author CHQ on 2022/7/27
 */
@Data
@ConfigurationProperties(prefix = WeiXinProperties.PREFIX)
public class WeiXinProperties {

    public static final String PREFIX = GlobalConstant.PREFIX + "weixin";

    /** 是否开启 */
    boolean enabled = true;

    /** 应用的唯一标识key */
    private String appKey = "";

    /** 应用的密钥 */
    private String appSecret = "";

    /** 是否打印操作日志 */
    private Boolean log = false;
}
