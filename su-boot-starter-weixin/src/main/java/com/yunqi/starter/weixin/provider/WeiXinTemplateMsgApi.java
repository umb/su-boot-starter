package com.yunqi.starter.weixin.provider;

import com.yunqi.starter.weixin.model.WeiXinResp;
import com.yunqi.starter.weixin.model.WeiXinTemplateMsg;

/**
 * Created by @author CHQ on 2023/11/18
 */
public interface WeiXinTemplateMsgApi {

    /**
     * 获取设置的行业信息
     * @return          请求结果
     */
    WeiXinResp getIndustry();

    /**
     * 设置所属行业
     * @param industryId1  公众号模板消息所属行业编号
     * @param industryId2  公众号模板消息所属行业编号
     * @return             请求结果
     */
    WeiXinResp setIndustry(String industryId1, String industryId2);

    /**
     * 获取模板列表
     * @return          请求结果
     */
    WeiXinResp getList();

    /**
     * 获得模板ID
     * @param templateIdShort   模板库中模板的编号，有“TM**”和“OPENTMTM**”等形式,对于类目模板，为纯数字ID
     * @return                  请求结果
     */
    WeiXinResp getId(String templateIdShort);

    /**
     * 删除模板
     * @param templateId    公众账号下模板消息ID, 包括类目模板ID
     * @return              请求结果
     */
    WeiXinResp del(String templateId);

    /**
     * 发送模板消息
     * @param msg           模板消息
     * @return              请求结果
     */
    WeiXinResp send(WeiXinTemplateMsg msg);

}
