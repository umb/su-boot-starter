package com.yunqi.starter.weixin.provider;


import com.yunqi.starter.core.lang.util.NutMap;

/**
 * Created by @author CHQ on 2022/2/28
 */
public interface WeiXinApi {

    /**
     * 获取凭证
     * @return 请求数据
     */
    NutMap getTicket();

}
