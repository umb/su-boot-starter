package com.yunqi.starter.weixin.spi;

import com.yunqi.starter.core.json.Json;
import com.yunqi.starter.core.lang.util.NutMap;
import com.yunqi.starter.weixin.WeiXinToken;
import com.yunqi.starter.weixin.configuration.WeiXinConfig;
import com.yunqi.starter.weixin.provider.WeiXinApi;
import com.yunqi.starter.weixin.provider.WeiXinTemplateMsgApi;
import com.yunqi.starter.weixin.provider.impl.WeiXinApiImpl;
import com.yunqi.starter.weixin.provider.impl.WeiXinTemplateMsgApiImpl;
import lombok.extern.slf4j.Slf4j;

/**
 * Created by @author CHQ on 2022/7/27
 */
@Slf4j
public class WeiXins {

    /**
     * 配置文件 Bean
     */
    public volatile static WeiXinConfig config;
    public static void setConfig(WeiXinConfig config) {
        WeiXins.config = config;
        if(config.getIsLog()){
            log.info("打印微信公众号配置如下 ->\n{}", Json.toJson(WeiXins.config));
        }
    }


    /**
     * 底层的 WxApi 对象
     */
    public static WeiXinApi api = new WeiXinApiImpl();

    public static WeiXinTemplateMsgApi templateMsgApi = new WeiXinTemplateMsgApiImpl();

    // =================== 获取Api 相关 ===================

    /**
     * 获取凭证
     * @return 微信公众号凭证
     */
    public static String getToken(){
        return WeiXinToken.getInstance().getAccessToken();
    }

    /**
     * 获取凭证
     * @return 请求数据
     */
    public static NutMap getTicket(){
        return api.getTicket();
    }

}
