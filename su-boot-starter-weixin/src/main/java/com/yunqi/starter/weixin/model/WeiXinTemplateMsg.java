package com.yunqi.starter.weixin.model;

import lombok.*;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * 模板消息
 * <p>
 * 参考 <a href="https://developers.weixin.qq.com/doc/offiaccount/Message_Management/Template_Message_Interface.html">接口说明</a>
 * Created by @author CHQ on 2022/11/13
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WeiXinTemplateMsg implements Serializable {

    private static final long serialVersionUID = 6491123872729710708L;

    /**
     * 公众号appid，要求与小程序有绑定且同主体.
     */
    private String appid;

    /**
     * 用户openid.
     * 可以是小程序的openid，也可以是mp_template_msg.appid对应的公众号的openid
     */
    private String touser;

    /**
     * 公众号模板ID.
     */
    private String template_id;

    /**
     * 公众号模板消息所要跳转的url.
     */
    private String url;

    /**
     * 公众号模板消息所要跳转的小程序，小程序的必须与公众号具有绑定关系.
     */
    private MiniProgram miniprogram;

    /**
     * 防重入id。对于同一个openid + client_msg_id, 只发送一条消息,10分钟有效,超过10分钟不保证效果。若无防重入需求，可不填
     */
    private String client_msg_id;

    /**
     * 小程序模板数据.
     */
    private Map<String, Object> data;

    public WeiXinTemplateMsg addData(String name, String value) {
        if (this.data == null) {
            this.data = new HashMap<>();
        }
        Map<String, String> map = new HashMap<>();
        map.put("value", value);
        this.data.put(name, map);
        return this;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class MiniProgram implements Serializable {

        private static final long serialVersionUID = -1277855564317016605L;

        private String appid;
        private String pagepath;
    }

}
