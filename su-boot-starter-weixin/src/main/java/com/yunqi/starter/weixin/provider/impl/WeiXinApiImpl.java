package com.yunqi.starter.weixin.provider.impl;

import com.yunqi.starter.core.json.Json;
import com.yunqi.starter.core.json.JsonFormat;
import com.yunqi.starter.core.lang.util.NutMap;
import com.yunqi.starter.weixin.provider.WeiXinApi;
import com.yunqi.starter.weixin.spi.WeiXins;
import com.yunqi.starter.weixin.util.WeiXinUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * Created by @author CHQ on 2022/2/28
 */
@Slf4j
public class WeiXinApiImpl implements WeiXinApi {

    public WeiXinApiImpl(){}


    @Override
    public NutMap getTicket() {
        String url = "/cgi-bin/stable_token";
        NutMap map = new NutMap();
        map.put("grant_type","client_credential");
        map.put("appid", WeiXins.config.getAppKey());
        map.put("secret", WeiXins.config.getAppSecret());
        map.put("force_refresh", false);
        return WeiXinUtil.post(url, Json.toJson(map, JsonFormat.tidy()));
    }

}
