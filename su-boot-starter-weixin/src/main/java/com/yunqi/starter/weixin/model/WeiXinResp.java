package com.yunqi.starter.weixin.model;

import com.yunqi.starter.core.lang.util.NutMap;

/**
 * Created by @author CHQ on 2022/7/27
 */
public class WeiXinResp extends NutMap {

    private static final long serialVersionUID = -1;

    public int errcode() {
        return getInt("errcode", 0);
    }

    public String errmsg() {
        return getString("errmsg");
    }


}
