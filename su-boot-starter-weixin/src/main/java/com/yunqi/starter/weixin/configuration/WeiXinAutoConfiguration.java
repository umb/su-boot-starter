package com.yunqi.starter.weixin.configuration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by @author CHQ on 2022/7/27
 */
@Slf4j
@Configuration
@ConditionalOnExpression("${" + WeiXinProperties.PREFIX + ".enabled:true}")
@EnableConfigurationProperties(WeiXinProperties.class)
public class WeiXinAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public WeiXinConfig getWeiXinConfig(WeiXinProperties properties) {
        if(properties.getLog()){
            log.info("微信公众号组件 -> 开启组件");
        }
        return new WeiXinConfig()
                .setAppKey(properties.getAppKey())
                .setAppSecret(properties.getAppSecret())
                .setIsLog(properties.getLog());
    }

}
