package com.yunqi.starter.weixin.util;

import com.yunqi.starter.core.exception.BizException;
import com.yunqi.starter.core.json.Json;
import com.yunqi.starter.core.json.JsonFormat;
import com.yunqi.starter.core.lang.Lang;
import com.yunqi.starter.core.lang.Strings;
import com.yunqi.starter.core.lang.util.NutMap;
import com.yunqi.starter.http.Https;
import com.yunqi.starter.weixin.model.WeiXinResp;
import com.yunqi.starter.weixin.spi.WeiXins;
import lombok.extern.slf4j.Slf4j;
import org.nutz.http.Request;
import org.nutz.http.Response;
import org.nutz.http.Sender;

import java.util.Map;

/**
 * 微信公众号内部工具类
 * Created by @author CHQ on 2022/6/19
 */
@Slf4j
public class WeiXinUtil {


    /**
     * 构建公共请求参数
     *
     * @param module     请求方法
     * @param body       请求参数
     * @return 返回正常请求参数
     */
    /*public static String call(String module, Map<String, Object> body) {

        String url = WeiXins.config.getGateway() + module;

        // --- 打印[请求参数] ----
        log.info("微信公众号组件 -> 请求地址:{},请求参数:{}", url, Json.toJson(body, JsonFormat.tidy()));
        WeiXinResp response = post(url, body);

        // --- 打印[请求结果] ----
        log.info("微信公众号组件 -> 响应数据:{}", Json.toJson(response, JsonFormat.tidy()));

        // 拦截异常提示
        *//*if (!response.getCode().equals("200") && !response.getCode().equals("00000")) {
            log.error("微信公众号组件 > 请求接口:{}, 错误提示:{}", method, response.getMessage());
            throw new BizException(response.getMessage());
        }*//*
        return response.errmsg();
    }*/

    /**
     * GET请求
     * @param url 请求地址
     * @return NutMap
     */
    public static NutMap get(String url){
        // 发起网络请求
        Request req = Request.create(WeiXins.config.getGateway() + url, Request.METHOD.GET);
        if(WeiXins.config.getIsLog()){
            log.info("打印[请求URL] ->\n{}", req.getUrl());
        }
        // 获取请求数据
        Response resp = Sender.create(req).send();
        NutMap res = Lang.map(resp.getContent());
        if(WeiXins.config.getIsLog()){
            log.info("打印[请求结果] ->\n{}", Json.toJson(res));
        }
        return res;
    }

    /**
     * POST请求
     * @param url    请求地址
     * @param data   请求数据
     * @return       map
     */
    public static NutMap post(String url, NutMap data){
        return post(url, Json.toJson(data));
    }

    public static NutMap post(String url, String data){
        // 发起网络请求
        Request req = Request.create(WeiXins.config.getGateway() + url, Request.METHOD.POST);

        // 设置Json请求格式
        req.getHeader().set("Content-Type", "application/json");

        if(WeiXins.config.getIsLog()){
            log.info("打印[请求URL] ->\n{}", req.getUrl());
        }
        // 设置请求Json数据
        if(Strings.isNotBlank(data)){
            req.setData(data);

            if(WeiXins.config.getIsLog()){
                log.info("打印[请求参数] ->\n{}", Json.toJson(Lang.map(data)));
            }
        }
        // 获取请求数据
        Response resp = Sender.create(req).send();
        NutMap res = Lang.map(resp.getContent());
        if(WeiXins.config.getIsLog()){
            log.info("打印[请求结果] ->\n{}", Json.toJson(res));
        }

        if (!resp.isOK()){
            throw new IllegalArgumentException("resp code=" + resp.getStatus());
        }


        // 拦截异常提示
        if(res.getInt("errcode", 0) != 0){
            throw new RuntimeException("请求微信公众号接口失败,异常代码:" + res.getInt("errcode")+ ",原因:" + res.getString("errmsg"));
        }
        return res;
    }

    /**
     * 构建令牌
     * @return  构建令牌字符串
     */
    public static String buildToken(){
        return String.format("?access_token=%s", WeiXins.getToken());
    }


    /**
     * 构建应用密钥
     * @return  构建应用密钥字符串
     */
    public static String buildAppKey(){
        return String.format("appid=%s&secret=%s", WeiXins.config.getAppKey(), WeiXins.config.getAppSecret());
    }

}
