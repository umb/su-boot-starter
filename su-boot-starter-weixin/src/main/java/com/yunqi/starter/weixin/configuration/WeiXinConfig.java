package com.yunqi.starter.weixin.configuration;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 配置类 Model
 * <p>
 * 你可以通过yml、properties、java代码等形式配置本类参数
 * Created by @author CHQ on 2022/7/27
 */
@Data
@Accessors(chain = true)
public class WeiXinConfig {

    private static final long serialVersionUID = 1L;

    /** 网关地址 */
    private String gateway = "https://api.weixin.qq.com";

    /** 应用的唯一标识key */
    private String appKey = "";

    /** 应用的密钥 */
    private String appSecret = "";

    /** 是否打印操作日志 */
    private Boolean isLog = true;

}
