package com.yunqi.starter.weixin.provider.impl;

import com.yunqi.starter.core.json.Json;
import com.yunqi.starter.weixin.model.WeiXinResp;
import com.yunqi.starter.weixin.model.WeiXinTemplateMsg;
import com.yunqi.starter.weixin.provider.WeiXinTemplateMsgApi;
import com.yunqi.starter.weixin.util.WeiXinUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * Created by @author CHQ on 2022/2/28
 */
@Slf4j
public class WeiXinTemplateMsgApiImpl implements WeiXinTemplateMsgApi {

    public WeiXinTemplateMsgApiImpl(){}


    @Override
    public WeiXinResp getIndustry() {
        return null;
    }

    @Override
    public WeiXinResp setIndustry(String industryId1, String industryId2) {
        return null;
    }

    @Override
    public WeiXinResp getList() {
        return null;
    }

    @Override
    public WeiXinResp getId(String templateIdShort) {
        return null;
    }

    @Override
    public WeiXinResp del(String templateId) {
        return null;
    }

    @Override
    public WeiXinResp send(WeiXinTemplateMsg msg) {
        WeiXinUtil.post("/cgi-bin/message/template/send" + WeiXinUtil.buildToken(),  Json.toJson(msg));
        return null;
    }
}
