package com.yunqi;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * 启动程序
 * Created by @author CHQ on 2023/7/10
 */
@EnableAsync // 开启异步方法
@SpringBootApplication
public class TestApplication {

    public static void main(String[] args)
    {
        SpringApplication app = new  SpringApplication(TestApplication.class);
        app.setBannerMode(Banner.Mode.LOG);
        app.run(args);
    }
}
