package com.yunqi;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.TimeInterval;
import com.yunqi.starter.core.codec.Base64;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * Base64 - 单元测试
 * Created by @author CHQ on 2023/7/10
 */
@Slf4j
@SpringBootTest
public class Base64Test {



    @Test
    public void Test() {
        TimeInterval timer = DateUtil.timer();
        log.info("任务开始");
        // --- 任务 begin -------------------------------------------------------------------------------------


        String a = "一个非常长的字符串";
        // 5Lym5a625piv5LiA5Liq6Z2e5bi46ZW/55qE5a2X56ym5Liy
        String encode = Base64.encode(a);

        log.info("打印数据 -> {}", encode);

        // 还原为a
        String decodeStr = Base64.decodeStr(encode);

        log.info("打印数据 -> {}", decodeStr);

        // --- 任务 end -------------------------------------------------------------------------------------
        log.warn("打印耗时:{}s" , timer.intervalSecond());
    }



}
