package com.yunqi;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.TimeInterval;
import com.yunqi.starter.core.lang.Randoms;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * 单元测试 - 随机数算法
 * Created by @author CHQ on 2023/7/10
 */
@Slf4j
@SpringBootTest
public class RandomTest {

    /**
     * ID算法
     */
    @Test
    public void test() {
        TimeInterval timer = DateUtil.timer();
        // --- 任务 begin -------------------------------------------------------------------------------------





        /*int c = Randoms.randomInt(10, 100);

        System.out.println(c);
        System.out.println(Randoms.randomString(1100));*/


        // --- 任务 end -------------------------------------------------------------------------------------
        log.warn("打印耗时:{}s" , timer.intervalSecond());
    }


}
