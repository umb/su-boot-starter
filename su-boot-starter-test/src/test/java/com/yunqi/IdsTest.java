package com.yunqi;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.TimeInterval;
import com.yunqi.starter.core.lang.Ids;
import com.yunqi.starter.core.lang.id.ObjectId;
import com.yunqi.starter.core.lang.id.Snowflake;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * 单元测试 - ID生成算法
 * Created by @author CHQ on 2023/7/10
 */
@Slf4j
@SpringBootTest
public class IdsTest {

    /**
     * ID算法
     */
    @Test
    public void test() {
        TimeInterval timer = DateUtil.timer();
        // --- 任务 begin -------------------------------------------------------------------------------------

        String uuid = Ids.randomUUID();

        String simpleUUID = Ids.simpleUUID();

        System.out.println(uuid);
        System.out.println(simpleUUID);

        String id = ObjectId.next();
        String id2 = Ids.objectId();

        System.out.println(id);
        System.out.println(id2);

        Snowflake snowflake = Ids.getSnowflake(1, 1);
        long id344 = snowflake.nextId();
        System.out.println(id344);

        long id1 = Ids.getSnowflakeNextId();
        String id12 = Ids.getSnowflakeNextIdStr();

        // --- 任务 end -------------------------------------------------------------------------------------
        log.warn("打印耗时:{}s" , timer.intervalSecond());
    }


}
