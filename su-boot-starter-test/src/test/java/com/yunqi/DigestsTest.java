package com.yunqi;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.TimeInterval;
import cn.hutool.crypto.digest.DigestUtil;
import cn.hutool.crypto.digest.HMac;
import cn.hutool.crypto.digest.HmacAlgorithm;
import com.yunqi.starter.core.crypto.digest.DigestAlgorithm;
import com.yunqi.starter.core.crypto.digest.Digester;
import com.yunqi.starter.core.lang.Digests;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * 单元测试 - 摘要算法
 * Created by @author CHQ on 2023/7/10
 */
@Slf4j
@SpringBootTest
public class DigestsTest {


    /**
     * 摘要算法
     */
    @Test
    public void digesterTest() {
        TimeInterval timer = DateUtil.timer();
        // --- 任务 begin -------------------------------------------------------------------------------------
        String content = "你好";

        System.out.println("MD5 加密 -> " + new Digester(DigestAlgorithm.MD5).digestHex(content));
        System.out.println("MD2 加密 -> " + new Digester(DigestAlgorithm.MD2).digestHex(content));
        System.out.println("SHA1 加密 -> " + new Digester(DigestAlgorithm.SHA1).digestHex(content));
        System.out.println("SHA256 加密 -> " + new Digester(DigestAlgorithm.SHA256).digestHex(content));
        System.out.println("SHA384 加密 -> " + new Digester(DigestAlgorithm.SHA384).digestHex(content));
        System.out.println("SHA512 加密 -> " + new Digester(DigestAlgorithm.SHA512).digestHex(content));
        System.out.println("SM3 加密 -> " + new Digester(DigestAlgorithm.SM3).digestHex(content));


        System.out.println("MD5 加密 -> " + Digests.md5Hex(content));
        System.out.println("MD5 16位 加密 -> " + Digests.md5Hex16(content));
        System.out.println("MD5 16位 加密 -> " + DigestUtil.md5Hex16(content));
        System.out.println("SHA1 加密 -> " + Digests.sha1Hex(content));
        System.out.println("SHA256 加密 -> " + Digests.sha256Hex(content));
        System.out.println("SHA512 加密 -> " + Digests.sha512Hex(content));
        System.out.println("SM3 加密 -> " + Digests.sm3Hex(content));

        // --- 任务 end -------------------------------------------------------------------------------------
        log.warn("打印耗时:{}s" , timer.intervalSecond());
    }

    /**
     * Bcrypt算法
     */
    @Test
    public void BcryptTest() {
        TimeInterval timer = DateUtil.timer();
        // --- 任务 begin -------------------------------------------------------------------------------------

        String password = "88888";
        String hashed = Digests.bcrypt(password);

        log.info("bcrypt 密文 -> {}", password);
        log.info("bcrypt 加密 -> {}", hashed);
        log.info("bcrypt 验证 -> {}", Digests.bcryptCheck(password, hashed));

        // --- 任务 end -------------------------------------------------------------------------------------
        log.warn("打印耗时:{}s" , timer.intervalSecond());
    }


    /**
     * 消息认证码算法
     */
    @Test
    public void HmacTest() {
        TimeInterval timer = DateUtil.timer();
        // --- 任务 begin -------------------------------------------------------------------------------------

        String testStr = "你好";

        // 此处密钥如果有非ASCII字符，考虑编码
        byte[] key = "password".getBytes();

        // b977f4b13f93f549e06140971bded384
        String macHex1 = new HMac(HmacAlgorithm.HmacMD5, key).digestHex(testStr);


        log.info("MACHEX1 加密 -> {}", macHex1);


        // --- 任务 end -------------------------------------------------------------------------------------
        log.warn("打印耗时:{}s" , timer.intervalSecond());
    }


}
