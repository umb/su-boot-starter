package com.yunqi;

import cn.hutool.core.date.TimeInterval;
import com.yunqi.starter.core.utils.DateUtil;
import com.yunqi.starter.weixin.model.WeiXinTemplateMsg;
import com.yunqi.starter.weixin.spi.WeiXins;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * 单元测试
 * Created by @author CHQ on 2023/7/10
 */
@Slf4j
@SpringBootTest
public class WeiXinTest {


    @Test
    public void Test2() throws Exception {
        TimeInterval timer = DateUtil.timer();
        // --- 任务 begin -------------------------------------------------------------------------------------

        // 当前日期字符串
        String beginDate = DateUtil.today();
        String endDate = DateUtil.formatDate(DateUtil.endOfMonth(DateUtil.date()));

        String templateId = "8nl_w5LnN1SDxBFDtEP9lDfvgb7W9WNOgdjYDh6DDzQ";

        WeiXinTemplateMsg msg = new WeiXinTemplateMsg();
        msg.setAppid(WeiXins.config.getAppKey());
        msg.setTouser("oyugqwOImbiA6FOK0RLVA4y_trek");
        msg.setTemplate_id(templateId);
        msg.setUrl("https://www.aliwork.com/o/dingc1c39818ebe4ad77");
        msg.addData("keyword1", "购物评价");
        msg.addData("keyword2", beginDate);
        msg.addData("keyword3", endDate);


        // 推送 -> 公众号消息通知
        WeiXins.templateMsgApi.send(msg);
        log.info("微信公众号凭证 -> {}", WeiXins.getToken());


        // --- 任务 end -------------------------------------------------------------------------------------
        log.warn("打印耗时:{}s" , timer.intervalSecond());
    }


    @Test
    public void Test() throws Exception {
        TimeInterval timer = DateUtil.timer();
        // --- 任务 begin -------------------------------------------------------------------------------------

        log.info("微信公众号凭证 -> {}", WeiXins.getToken());


        // --- 任务 end -------------------------------------------------------------------------------------
        log.warn("打印耗时:{}s" , timer.intervalSecond());
    }



}
