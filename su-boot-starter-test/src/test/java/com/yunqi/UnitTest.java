package com.yunqi;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.TimeInterval;
import com.yunqi.starter.core.crypto.digest.DigestAlgorithm;
import com.yunqi.starter.core.crypto.digest.Digester;
import com.yunqi.starter.core.lang.Charsets;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * 单元测试
 * Created by @author CHQ on 2023/7/10
 */
@Slf4j
@SpringBootTest
public class UnitTest {



    @Test
    public void Test() throws Exception {
        TimeInterval timer = DateUtil.timer();
        // --- 任务 begin -------------------------------------------------------------------------------------
        Digester md5 = new Digester(DigestAlgorithm.MD5);
        String md5Str =  md5.digestHex("你好");
        log.info("MD5加密 -> {}", md5Str);


        // --- 任务 end -------------------------------------------------------------------------------------
        log.warn("打印耗时:{}s" , timer.intervalSecond());
    }



}
