package com.yunqi;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.TimeInterval;
import com.yunqi.starter.core.crypto.digest.DigestAlgorithm;
import com.yunqi.starter.core.crypto.digest.Digester;
import com.yunqi.starter.core.json.Json;
import com.yunqi.starter.http.Https;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;

/**
 * 单元测试
 * Created by @author CHQ on 2023/7/10
 */
@Slf4j
@SpringBootTest
public class HttpTest {



    @Test
    public void Test() throws Exception {
        TimeInterval timer = DateUtil.timer();
        // --- 任务 begin -------------------------------------------------------------------------------------


        // --- 任务 end -------------------------------------------------------------------------------------
        log.warn("打印耗时:{}s" , timer.intervalSecond());
    }



}
