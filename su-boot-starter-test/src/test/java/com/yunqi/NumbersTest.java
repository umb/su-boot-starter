package com.yunqi;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.TimeInterval;
import com.yunqi.starter.core.lang.Numbers;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * 数学运算 - 单元测试
 * Created by @author CHQ on 2023/7/10
 */
@Slf4j
@SpringBootTest
public class NumbersTest {



    @Test
    public void add() {
        TimeInterval timer = DateUtil.timer();
        log.warn("任务开始");
        // --- 任务 begin -------------------------------------------------------------------------------------

        double te1= 123456.123456;
        double te2= 123456.128456;

        double num = Numbers.add(te1, te2);

        log.info("加法运算 -> {}", num);

        log.info("减法运算 -> {}", Numbers.sub(num, te2));

        log.info("乘法运算 -> {}", Numbers.mul(te1, 2));

        log.info("除法运算 -> {}", Numbers.div(num, 2));

        log.info("除法运算 -> {}", Numbers.div(num, 2,2));

        log.info("除法运算 -> {}", Numbers.isNumber(""));

        // --- 任务 end -------------------------------------------------------------------------------------
        log.warn("打印耗时:{}s" , timer.intervalSecond());
    }



}
