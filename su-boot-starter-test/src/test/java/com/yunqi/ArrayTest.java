package com.yunqi;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.TimeInterval;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.URLUtil;
import cn.hutool.http.HttpUtil;
import com.yunqi.starter.core.lang.Arrays;
import com.yunqi.starter.core.lang.Objects;
import com.yunqi.starter.http.Https;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * 单元测试 - 数组
 * Created by @author CHQ on 2023/7/10
 */
@Slf4j
@SpringBootTest
public class ArrayTest {

    @Test
    public void test() {
        TimeInterval timer = DateUtil.timer();
        // --- 任务 begin -------------------------------------------------------------------------------------

        String content = Https.get("");


        // --- 任务 end -------------------------------------------------------------------------------------
        log.warn("打印耗时:{}s" , timer.intervalSecond());
    }


}
