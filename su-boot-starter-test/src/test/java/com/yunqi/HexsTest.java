package com.yunqi;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.TimeInterval;
import com.yunqi.starter.core.lang.Hexs;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * 十六进制 - 单元测试
 * Created by @author CHQ on 2023/7/10
 */
@Slf4j
@SpringBootTest
public class HexsTest {



    @Test
    public void Test() {
        TimeInterval timer = DateUtil.timer();
        log.info("任务开始");
        // --- 任务 begin -------------------------------------------------------------------------------------



        String str = "我是一个字符串";
        log.info("字符串 -> {}", str);

        String hex = Hexs.encodeHexStr(str);
        log.info("十六进制字符串 -> {}", hex);


        //hex是：
        //e68891e698afe4b880e4b8aae5ad97e7aca6e4b8b2

        String decodedStr = Hexs.decodeHexStr(hex);
        log.info("字符串 -> {}", decodedStr);


        // --- 任务 end -------------------------------------------------------------------------------------
        log.warn("打印耗时:{}s" , timer.intervalSecond());
    }



}
