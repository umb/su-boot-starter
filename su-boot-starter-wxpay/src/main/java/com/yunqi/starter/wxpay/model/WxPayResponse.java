package com.yunqi.starter.wxpay.model;

import lombok.Data;
import lombok.experimental.Accessors;
import org.nutz.http.Header;

import java.io.Serializable;

/**
 * 微信支付V3接收类
 * Created by @author CHQ on 2022/9/17
 */
@Data
@Accessors(chain = true)
public class WxPayResponse implements Serializable {

    private static final long serialVersionUID = 1L;
    private String body;
    private int status;
    private Header header;

}
