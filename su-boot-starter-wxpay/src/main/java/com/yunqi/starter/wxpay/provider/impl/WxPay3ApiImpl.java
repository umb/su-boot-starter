package com.yunqi.starter.wxpay.provider.impl;

import com.yunqi.starter.core.json.Json;
import com.yunqi.starter.core.lang.util.NutMap;
import com.yunqi.starter.wxpay.model.WxPayCertificatesResponse;
import com.yunqi.starter.wxpay.model.WxPayResponse;
import com.yunqi.starter.wxpay.provider.WxPay3Api;
import com.yunqi.starter.wxpay.spi.WxPays;
import com.yunqi.starter.wxpay.util.WxPayUtil;

/**
 * Created by @author CHQ on 2022/9/20
 */
public class WxPay3ApiImpl implements WxPay3Api {

    @Override
    public String getCertificates() throws Exception {
        // 获取微信平台证书
        WxPayCertificatesResponse certificatesResponse = certificates();
        WxPayCertificatesResponse.Certificate.EncryptCertificate certificate = certificatesResponse.getData().get(0).getEncrypt_certificate();
        // 返回解密后的平台证书
        return WxPayUtil.decryptToString(certificate.getAssociated_data(), certificate.getNonce(), certificate.getCiphertext());
    }

    @Override
    public WxPayCertificatesResponse certificates() throws Exception {
        String url = "/v3/certificates";
        WxPayResponse resp = WxPayUtil.call("GET", url, "");
        return Json.fromJson(WxPayCertificatesResponse.class, resp.getBody());
    }

    @Override
    public WxPayResponse order_jsapi(String body) throws Exception {
        String url = "/v3/pay/transactions/jsapi";
        return WxPayUtil.call("POST", url,  body);
    }

    @Override
    public WxPayResponse order_app(String body) throws Exception {
        String url = "/v3/pay/transactions/app";
        return WxPayUtil.call("POST", url, body);
    }

    @Override
    public WxPayResponse order_native(String body) throws Exception {
        String url = "/v3/pay/transactions/native";
        return WxPayUtil.call("POST", url,  body);
    }

    @Override
    public WxPayResponse order_h5(String body) throws Exception {
        String url = "/v3/pay/transactions/h5";
        return WxPayUtil.call("POST", url, body);
    }

    @Override
    public WxPayResponse order_close(String out_trade_no) throws Exception {
        String url = "/v3/pay/transactions/out-trade-no/" + out_trade_no + "/close";
        String body = Json.toJson(NutMap.NEW().addv("mchid", WxPays.config.getMchId()));
        return WxPayUtil.call("POST", url,  body);
    }

    @Override
    public WxPayResponse order_query_transaction_id(String transaction_id) throws Exception {
        String url = "/v3/pay/transactions/id/" + transaction_id + "?mchid=" + WxPays.config.getMchId();
        return WxPayUtil.call("GET", url,"");
    }

    @Override
    public WxPayResponse order_query_out_trade_no(String out_trade_no) throws Exception {
        String url = "/v3/pay/transactions/out-trade-no/" + out_trade_no + "?mchid=" + WxPays.config.getMchId();
        return WxPayUtil.call("GET", url, "");
    }

    @Override
    public WxPayResponse refund(String body) throws Exception {
        String url = "/v3/refund/domestic/refunds";
        return WxPayUtil.call("POST", url,  body);
    }

    @Override
    public WxPayResponse ecommerce_refunds_apply(String body) throws Exception {
        String url = "/v3/ecommerce/refunds/apply";
        return WxPayUtil.call("POST", url,  body);
    }

    @Override
    public WxPayResponse ecommerce_refunds_query_refund_id(String refund_id) throws Exception {
        String url = "/v3/ecommerce/refunds/id/" + refund_id + "?sub_mchid=" + WxPays.config.getMchId();
        return WxPayUtil.call("GET", url,"");
    }

    @Override
    public WxPayResponse ecommerce_refunds_query_out_refund_no(String out_refund_no) throws Exception {
        String url = "/v3/ecommerce/refunds/out-refund-no/" + out_refund_no + "?sub_mchid=" + WxPays.config.getMchId();
        return WxPayUtil.call("GET", url,  "");
    }
}
