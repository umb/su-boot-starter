package com.yunqi.starter.wxpay.configuration;

import com.yunqi.starter.core.constant.GlobalConstant;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 微信支付v3配置
 * Created by @author CHQ on 2022/9/18
 */
@Getter
@Setter
@ConfigurationProperties(prefix = WxPayProperties.PREFIX)
public class WxPayProperties {

    public static final String PREFIX = GlobalConstant.PREFIX + "wxpay";

    /** 是否开启 */
    boolean enabled = true;

    /**
     * 网关地址
     */
    private String gateway  = "https://api.mch.weixin.qq.com";

    /**
     * 应用编号
     */
    private String appId;

    /**
     * 商户号
     */
    private String mchId;

    /**
     * 商户平台「API安全」中的 API 密钥
     */
    private String apiKey;

    /**
     * 商户平台「API安全」中的 APIv3 密钥
     */
    private String apiKey3;

    /**
     * 回调域名中会使用此参数
     */
    private String callbackDomain;

    /**
     * API 证书中的 p12
     */
    private String certPath;

    /**
     * API 证书中的 key.pem
     */
    private String keyPemPath;

    /**
     * API 证书中的 cert.pem
     */
    private String certPemPath;

    /**
     * 平台证书
     */
    private String platformCertPath;

    /**
     * 其他附加参数
     */
    private Object exParams;

    /** 是否打印操作日志 */
    private Boolean log = false;

}
