package com.yunqi.starter.wxpay.model;

import lombok.Data;

import java.util.List;

/**
 * 平台证书
 * Created by @author CHQ on 2023/5/11
 */
@Data
public class WxPayCertificatesResponse {

    /**
     * 返回的证书列表
     */
    private List<Certificate> data;

    @Data
    public static class Certificate {
        /**
         * 证书的序列号
         */
        private String serial_no;

        /**
         * 证书生效时间，使用 ISO 8601 格式表示
         */
        private String effective_time;

        /**
         * 证书过期时间，使用 ISO 8601 格式表示
         */
        private String expire_time;

        /**
         * 加密证书信息
         */
        private EncryptCertificate encrypt_certificate;

        @Data
        public static class EncryptCertificate {
            /**
             * 证书加密算法
             */
            private String algorithm;

            /**
             * 加密时使用的 nonce
             */
            private String nonce;

            /**
             * 加密时使用的关联数据
             */
            private String associated_data;

            /**
             * 表示加密后的证书信息的密文
             */
            private String ciphertext;
        }
    }
}
