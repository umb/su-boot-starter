package com.yunqi.starter.security.configuration;

import cn.dev33.satoken.config.SaTokenConfig;
import cn.dev33.satoken.spring.SaBeanRegister;
import cn.dev33.satoken.stp.StpLogic;
import com.yunqi.starter.core.json.Json;
import com.yunqi.starter.security.spi.TokenUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * 应用端认证
 * Created by @author CHQ on 2022/2/16
 */
@Slf4j
@Configuration
@ConditionalOnClass(SaTokenConfig.class)
@ConditionalOnExpression("${" + SecurityTokenProperties.PREFIX +".enabled:true}")
@EnableConfigurationProperties(SecurityTokenProperties.class)
@AutoConfigureAfter({SaBeanRegister.class})
public class SecurityTokenAutoConfiguration {

    // 安全属性对象
    private final SecurityTokenProperties properties;
    public SecurityTokenAutoConfiguration(SecurityTokenProperties properties) {
        this.properties = properties;
    }

    @Bean
    public void tokenUtil() {
        // 获取配置
        SaTokenConfig config = new SaTokenConfig();
        config.setIsLog(properties.getLog());
        BeanUtils.copyProperties(properties, config);

        if(properties.getLog()){
            log.info("打印 -> security.token 配置:\n{}", Json.toJson(config));
        }

        // 重写 stpLogic 配置获取方法
        StpLogic stpLogic =  new StpLogic(TokenUtil.TYPE) {
            @Override
            public SaTokenConfig  getConfig() {
                return config;
            }
        };
        TokenUtil.setStpLogic(stpLogic);
    }

}
