package com.yunqi.starter.wx;


import com.yunqi.starter.wx.spi.Wxs;
import lombok.Data;

import java.util.Map;

/**
 * 微信小程序 accessToken
 * 单例设计模式 缓存
 * Created by @author CHQ on 2022/6/19
 */
@Data
public class WxToken {

    /**
     * 访问凭据
     */
    private String accessToken;

    /**
     * 过期时间
     */
    private Long expiryTime;


    private static WxToken instance = new WxToken();

    /**
     * 私有构造函数，防止外部实例化对象
     */
    private WxToken() {}

    /**
     * 获取 WxToken 的实例，如果 accessToken 已过期，则刷新
     *
     * @return WxToken 实例
     */
    public static WxToken getInstance() {
        if (instance.isAccessTokenExpired()) {
            instance.refreshAccessToken();
        }
        return instance;
    }

    /**
     * 判断 accessToken 是否过期
     */
    private boolean isAccessTokenExpired() {
        return expiryTime == null || System.currentTimeMillis() >= expiryTime;
    }

    /**
     * 刷新 accessToken
     */
    private synchronized void refreshAccessToken() {
        // 从微信获取新的 access_token
        Map<String, Object> ticket = Wxs.getTicket();
        accessToken = (String) ticket.get("access_token");
        int  expiresIn = (Integer) ticket.get("expires_in");
        // 计算新的过期时间，至少为 5 分钟
        expiryTime = System.currentTimeMillis() + (expiresIn * 1000L - 300000);
    }
}
