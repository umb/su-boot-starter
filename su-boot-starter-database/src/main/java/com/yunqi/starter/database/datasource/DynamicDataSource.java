package com.yunqi.starter.database.datasource;

import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

/**
 * 动态数据源
 * Created by @author CHQ on 2023/6/27
 */
public class DynamicDataSource extends AbstractRoutingDataSource {


    /**
     * 构造函数，用于设置默认数据源和目标数据源
     * @param defaultTargetDataSource   默认数据源
     * @param targetDataSources         目标数据源
     */
    public DynamicDataSource(DataSource defaultTargetDataSource, Map<Object, Object> targetDataSources) {
        // 设置默认数据源
        super.setDefaultTargetDataSource(defaultTargetDataSource);
        // 设置目标数据源
        super.setTargetDataSources(targetDataSources);
        // 初始化数据源
        super.afterPropertiesSet();
    }

    /**
     * 重写 determineCurrentLookupKey() 方法，用于获取当前线程的数据源名称
     */
    @Override
    protected Object determineCurrentLookupKey() {
        return DynamicDataSourceContextHolder.getDataSource();
    }

}
