package com.yunqi.starter.database.enums;

/**
 * 数据源
 * Created by @author CHQ on 2023/6/27
 */
public enum DataSourceType {

    /**
     * 主库
     */
    MASTER,


    /**
     * 从库
     */
    SLAVE
}
