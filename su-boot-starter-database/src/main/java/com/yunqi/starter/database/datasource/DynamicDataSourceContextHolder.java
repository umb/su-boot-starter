package com.yunqi.starter.database.datasource;

import com.yunqi.starter.database.enums.DataSourceType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 数据源切换器
 * Created by @author CHQ on 2023/6/27
 */
public class DynamicDataSourceContextHolder {

    public static final Logger log = LoggerFactory.getLogger(DynamicDataSourceContextHolder.class);

    /**
     * 使用 ThreadLocal 变量保存当前线程的数据源名称
     */
    private static final ThreadLocal<DataSourceType> CONTEXT_HOLDER = new ThreadLocal<>();

    /**
     * 设置当前线程的数据源名称
     * @param dsType    数据源名称
     */
    public static void setDataSource(DataSourceType dsType) {
        // 打印切换数据源的日志信息
        log.info("数据库组件 -> 切换到{}数据源", dsType);
        // 设置当前线程的数据源名称
        CONTEXT_HOLDER.set(dsType);
    }

    /**
     * 获取当前线程的数据源名称
     */
    public static DataSourceType getDataSource() {
        return CONTEXT_HOLDER.get();
    }

    /**
     * 清除当前线程的数据源名称
     */
    public static void clearDataSource() {
        CONTEXT_HOLDER.remove();
    }

}
