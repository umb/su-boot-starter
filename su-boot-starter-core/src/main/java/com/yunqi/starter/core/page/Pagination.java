package com.yunqi.starter.core.page;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 分页模型
 * Created by @author CHQ on 2022/1/27
 */
@Data
public class Pagination<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 设置默认每页数为10
     */
    public static int DEFAULT_PAGE_SIZE = 10;

    //------------------------------------------------------------------------- attribute

    /**
     * 页码，默认设置为1时，页码1表示第一页
     */
    protected int page;

    /**
     * 每页数
     */
    protected int pageSize;

    /**
     *  总数
     */
    protected int totalCount;

    /**
     * 总页数
     */
    protected int totalPage = getTotalPage();

    /**
     * 数据列表
     */
    protected List<T> list = new ArrayList<>();


    //------------------------------------------------------------------------- build

    /**
     * 构造函数
     */
    public Pagination(){}


    /**
     * 构造函数
     * @param page  页码
     */
    public Pagination(int page) {
        if (page < 1) {
            page = 1;
        }
        this.page =  page;
        this.pageSize = DEFAULT_PAGE_SIZE;
    }

    /**
     * 构造函数
     * @param page      页码
     * @param pageSize  每页数
     */
    public Pagination(int page, int pageSize) {
        if (page < 1) {
            page = 1;
        }
        if (pageSize < 1) {
            pageSize = DEFAULT_PAGE_SIZE;
        }
        this.page = page;
        this.pageSize = pageSize;
    }

    private Pagination(Builder<T> builder) {
        this.page = builder.page;
        this.pageSize = builder.pageSize;
        this.totalCount = builder.totalCount;
        this.totalPage = (int) Math.ceil((double) totalCount / pageSize);
        this.list = builder.list;
    }

    //------------------------------------------------------------------------- get/set

    /**
     * 设置页码
     * @param page  页码
     */
    public Pagination<T> setPage(int page) {
        if (1 > page) {
            return this;
        }
        this.page = page;
        return this;
    }

    /**
     * 设置每页数
     */
    public Pagination<T> setPageSize(int pageSize) {
        this.pageSize = (pageSize > 0 ? pageSize : DEFAULT_PAGE_SIZE);
        return this;
    }

    /**
     * 设置总页数
     */
    public Pagination<T> setTotalCount(int totalCount) {
        this.totalCount = Math.max(totalCount, 0);
        this.totalPage = (int) Math.ceil((double) totalCount / pageSize);
        return this;
    }

    /**
     * 获取总页数
     */
    public int getTotalPage() {
        if (totalPage < 0) {
            totalPage = (int) Math.ceil((double) totalCount / pageSize);
        }
        return totalPage;
    }


    //------------------------------------------------------------------------- public method

    /**
     * 是否第一页
     */
    public boolean isFirstPage() {
        return page == 1;
    }

    /**
     * 是否最后一页
     */
    public boolean isLastPage() {
        if (totalPage == 0) {
            return true;
        }
        return page == totalPage;
    }

    /**
     * 返回下页的页号
     */
    public int getNextPage() {
        if (isLastPage()) {
            return page;
        } else {
            return page + 1;
        }
    }

    /**
     * 返回上页的页号
     */
    public int getPrePage() {
        if (isFirstPage()) {
            return page;
        } else {
            return page - 1;
        }
    }

    //------------------------------------------------------------------------- public builder

    public static <T> Builder<T> builder() {
        return new Builder<>();
    }

    public static <T> Builder<T> builder(int page, int pageSize) {
        return new Builder<>(page, pageSize);
    }

    public static class Builder<T> {
        private int page;
        private int pageSize;
        private int totalCount;
        private List<T> list;

        public Builder() {
        }

        public Builder(int page, int pageSize) {
            this.page = page;
            this.pageSize = pageSize;
        }

        public Builder<T> page(int page) {
            this.page = page;
            return this;
        }

        public Builder<T> pageSize(int pageSize) {
            this.pageSize = pageSize;
            return this;
        }

        public Builder<T> totalCount(int totalCount) {
            this.totalCount = totalCount;
            return this;
        }

        public Builder<T> list(List<T> list) {
            this.list = list;
            return this;
        }

        public Pagination<T> build() {
            return new Pagination<>(this);
        }
    }


}
