package com.yunqi.starter.core.crypto.digest;

/**
 * 摘要算法类型<br>
 * see: https://docs.oracle.com/javase/7/docs/technotes/guides/security/StandardNames.html#MessageDigest
 *
 * Created by @author CHQ on 2023/7/10
 */
public enum DigestAlgorithm {

    MD2("MD2"),
    MD5("MD5"),
    SHA1("SHA-1"),
    SHA256("SHA-256"),
    SHA384("SHA-384"),
    SHA512("SHA-512"),

    SM3("SM3");

    private final String value;

    /**
     * 构造
     *
     * @param value 算法字符串表示
     */
    DigestAlgorithm(String value) {
        this.value = value;
    }

    /**
     * 获取算法字符串表示
     * @return 算法字符串表示
     */
    public String getValue() {
        return this.value;
    }

}
