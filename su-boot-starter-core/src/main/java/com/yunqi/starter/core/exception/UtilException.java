package com.yunqi.starter.core.exception;

import cn.hutool.core.exceptions.ExceptionUtil;
import com.yunqi.starter.core.lang.Strings;

/**
 * 工具类异常
 * Created by @author CHQ on 2023/9/25
 */
public class UtilException extends RuntimeException {

    private static final long serialVersionUID = 8247610319171014183L;

    public UtilException(Throwable e) {
        super(ExceptionUtil.getMessage(e), e);
    }

    public UtilException(String message) {
        super(message);
    }

    public UtilException(String messageTemplate, Object... params) {
        super(Strings.format(messageTemplate, params));
    }

    public UtilException(String message, Throwable throwable) {
        super(message, throwable);
    }

    public UtilException(String message, Throwable throwable, boolean enableSuppression, boolean writableStackTrace) {
        super(message, throwable, enableSuppression, writableStackTrace);
    }

    public UtilException(Throwable throwable, String messageTemplate, Object... params) {
        super(Strings.format(messageTemplate, params), throwable);
    }

}
